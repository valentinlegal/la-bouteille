const $bottle = document.querySelector('.bottle');
let spins = 0;

const spin = () => {
  spins++;

  const random = Math.floor(Math.random() * 10);
  const rotation = spins * 720 + random * (360 / 10);

  $bottle.style.transform = 'rotate(' + rotation + 'deg)';
};
